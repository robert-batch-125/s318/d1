/*
	Server connect on MongoDB via Mongoose ODM

	1. Create the project folder called 'task-api'
	2. Initialize an npm by running command:

		$ npm init

	3. Create the app entry point file:

		$ touch index.js

	4. Install the packages for express, nodemon and mongoose by running command:

		$ nnpm install express nodemon mongoose

	5. Set up the initial codes for creating  a server.
	6. Open your MongoDB atlas account then 
	7. Get the conection string on the MongoDB Atlas and update its passsword and the DB name:

		sample connection string -
								// password                         //db name
		mongodb+srv://robertdaveson:Superman@cluster0.sck3f.mongodb.net/b125-tasks?retryWrites=true&w=majority

	8. Use the connection string to connect our  MongoDB Atlas to our server


*/